
package cz.muni.fi.xklinec.phoenixdbgen.db;

import java.util.Date;

/**
 * This is sample class that will be converted to android database class.
 * @author ph4r05
 */
public class DB2Gen {
    protected Integer id;
    
    
    protected Integer account;
    protected String sip;
    protected String displayName;
    protected byte[] certificate;
    protected String certificateHash;
    protected boolean inWhitelist;
    protected Date dateCreated;
    protected Date dateLastModified;
    protected boolean presenceOnline = false;
    protected String presenceStatus = null;
    protected Date presenceLastUpdate;

    protected Integer presenceStatusType;
    protected String presenceStatusText;
    protected String presenceCertHashPrefix;
    protected Date presenceCertNotBefore;

    protected Date presenceLastCertUpdate;
    protected Integer presenceNumCertUpdate;
    protected String presenceDosCertUpdate;

    protected Integer unreadMessages;
}
