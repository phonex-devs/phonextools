package cz.muni.fi.xklinec.phoenixdbgen;

import com.google.common.base.CaseFormat;
import com.google.common.base.Predicate;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.reflections.Reflections;
import org.reflections.scanners.ConvertersScanner;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

/**
 * Hello world!
 *
 */
public class App 
{
    public static final String AppPack = "net.phonex";
    public static final String packDest = "com.csipsimple.api";
    public static final String pack = "cz.muni.fi.xklinec.phoenixdbgen.db";
    
    public static void main( String[] args ){
        
        System.out.println( "Hello World from DB creator!" );
        Reflections.collect();
        
        //Reflections reflections = new Reflections(""+AppPack+".soap.beans");
         Predicate<String> filter = new FilterBuilder().include(""+AppPack+".soap.beans\\$.*");
         Reflections reflections = new Reflections(new ConfigurationBuilder()
                .filterInputsBy(filter)
                .setScanners(
                        new SubTypesScanner().filterResultsBy(filter),
                        new TypeAnnotationsScanner().filterResultsBy(filter),
                        new FieldAnnotationsScanner().filterResultsBy(filter),
                        new MethodAnnotationsScanner().filterResultsBy(filter),
                        new ConvertersScanner().filterResultsBy(filter)));
         
        Reflections.collect();
        reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(
                       new SubTypesScanner(false), 
                       new ResourcesScanner(),
                       new FieldAnnotationsScanner(),
                       new TypeAnnotationsScanner(),
                       new MethodAnnotationsScanner(),
                       new ConvertersScanner())
                .setUrls(ClasspathHelper.forPackage(pack))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(pack)))); // new Reflections(pack);
        
        Reflections.collect();
        
        // Get all classes in given object.
        Set<Class<? extends Object>> allClasses =  reflections.getSubTypesOf(Object.class);
        System.out.println("Classes found: " + allClasses.size());
        for(Class<? extends Object> cls : allClasses){
            System.out.println("Class: " + cls.getCanonicalName());
            
            // Process inside.
            String newClass = reconstructClass(cls);
            System.err.println(newClass);
        }
    }
    
    public static class FieldConst {
        public String attrName;
        public String constName;
        public FieldConst(String attrName, String constName) {
            this.attrName = attrName;
            this.constName = constName;
        }
    }
    
    public static class FieldType {
        public String attrName;
        public String stype;
        public FieldType(String attrName, String stype) {
            this.attrName = attrName;
            this.stype = stype;
        }
    }
    
    public static String reconstructClass(Class<?> en){
        StringBuilder sb = new StringBuilder();
        String can = en.getCanonicalName();
        boolean hasByteArray=false;
        boolean hasDate=false;
        boolean hasInteger=false;
        boolean hasLong=false;
        boolean hasCalendar=false;
        boolean hasDouble=false;
        boolean hasFloat=false;
        boolean hasString=false;
        
        // Maximum length of the attribute name in characters.
        int maxAttrLen = 0;
        // Maximum length of the FIELD_ constant name in characters.
        int maxConstLen = 0;
        
        // 
        Map<String, String> attributesFromSamePackage = new HashMap<String, String>();
        
        // attribute name to index
        Map<String, Integer> attr2idx = new HashMap<String, Integer>();
        
        // Field name to FIELD_ constant name.
        Map<String, String> field2const = new HashMap<String, String>();
        List<FieldConst> fieldConstList = new LinkedList<FieldConst>();
        
        // Field name to field type mapping.
        Map<String, String> field2type = new HashMap<String, String>();
        List<FieldType> fieldTypeList  = new LinkedList<FieldType>();
        
        System.out.println("Reconstructing class " + en.getSimpleName());
        
        // declaration
        sb.append("\n\n<CLASS>\n");
        sb.append("package "+packDest+";\n\n");
        sb.append(  "import java.io.ByteArrayInputStream;\n" +
                    "import java.io.InputStream;\n" +
                    "import java.security.cert.CertificateEncodingException;\n" +
                    "import java.security.cert.CertificateException;\n" +
                    "import java.security.cert.CertificateFactory;\n" +
                    "import java.security.cert.X509Certificate;\n" +
                    "import java.text.ParseException;\n" +
                    "import java.text.SimpleDateFormat;\n" +
                    "import java.util.Arrays;\n" +
                    "import java.util.Date;\n" +
                    "import java.util.Locale;\n" +
                    "\n" +
                    "import android.content.ContentProvider;\n" +
                    "import android.content.ContentResolver;\n" +
                    "import android.content.ContentUris;\n" +
                    "import android.content.ContentValues;\n" +
                    "import android.content.Context;\n" +
                    "import android.database.Cursor;\n" +
                    "import android.database.DatabaseUtils;\n" +
                    "import android.net.Uri;\n" +
                    "import android.os.Parcel;\n" +
                    "import android.os.Parcelable;\n" +
                    "\n" +
                    "import com.csipsimple.utils.Log;\n\n\n");
        
        sb.append("public class ").append(en.getSimpleName()).append(" implements Parcelable {\n\n");
        //sb.append("    public final static String NAMESPACE = \"http://phoenix.com/hr/schemas\";\n");
        
        // fields declaration
        Field[] fields = en.getDeclaredFields();
        int i = 0, sz = fields.length;
        for(i = 0; i < sz; i++){
            Field fld = fields[i];
            String f = fld.getName();
            Class<?> ftype = fld.getType();
            String t = ftype.getCanonicalName();
            String stype = ftype.getSimpleName();
            
            // special serializers
            if ("byte[]".equalsIgnoreCase(t)){
                hasByteArray=true;
            } else if("java.util.Date".equalsIgnoreCase(t)){
                hasDate=true; hasLong=true;
            } else if("java.lang.Double".equalsIgnoreCase(t)){
                hasDouble=true;
            } else if("java.lang.Float".equalsIgnoreCase(t)){
                hasFloat=true;
            } else if("java.lang.Integer".equalsIgnoreCase(t)){
                hasInteger=true;
            } else if("java.lang.Long".equalsIgnoreCase(t)){
                hasLong=true;
            } else if("java.lang.String".equalsIgnoreCase(t)){
                hasString=true;
            }
            
            if (t.startsWith(pack) || t.startsWith(packDest)){
                attributesFromSamePackage.put(f, t.replace(pack, packDest));
            }
            
            // convert cammel case to underscore case
            String under = camelCase2Underscore(f);
            String field = "FIELD_" + under.toUpperCase();
            field2const.put(f, field);
            fieldConstList.add(new FieldConst(f, field));
            
            String type = t.replace(pack, packDest);
            System.err.println("  rField: canon=" + type + "; simple=" + stype + "; under=" + under);
            
            field2type.put(f, stype);
            fieldTypeList.add(new FieldType(f, stype));
            
            attr2idx.put(fld.getName(), Integer.valueOf(i));
            
            int tmpAlen = f.length();
            int tmpClen = field.length();
            
            if (tmpAlen > maxAttrLen) maxAttrLen = tmpAlen;
            if (tmpClen > maxConstLen) maxConstLen = tmpClen;
        }
        
        String indent = "    ";
        
        // Log tag
        sb.append(indent).append("public final static String THIS_FILE = \""+en.getSimpleName()+"\";\n");
        
        // Table name
        sb.append(indent).append("public final static String TABLE_NAME = \""+en.getSimpleName()+"\";\n");
        
        // Invalid primary key
        sb.append(indent).append("public final static long INVALID_ID = -1;\n\n");
        
        // Generate field text info
        sb.append("    // <FIELD_NAMES>\n");
        for(FieldConst e : fieldConstList){
            String pcst = fill(e.constName, maxConstLen + 1, " ");
            
            sb.append(indent).append("public static final String ")
                    .append(pcst)
                    .append(" = \"")
                    .append(e.attrName)
                    .append("\";\n");
        } sb.append("    // </FIELD_NAMES>\n"); sb.append("\n");
        
        // Generate protected fields
        sb.append("    // <ATTRIBUTES>\n");
        for(FieldType e : fieldTypeList){
            sb.append(indent).append("protected ")
                    .append(e.stype)
                    .append(" ")
                    .append(e.attrName)
                    .append(";\n");
        } sb.append("    // </ATTRIBUTES>\n"); sb.append("\n");
        
        // Full projection
        sb.append(indent).append("public static final String[] FULL_PROJECTION = new String[] {\n        ");
        i = 1;
        for(FieldConst e : fieldConstList){
            sb.append(e.constName).append(", ");
            if ((i%2) == 0 && i>0) sb.append("\n        ");
            i+=1;
        }
        sb.append("\n    };\n\n");    
        
        // Create table
        sb.append(indent).append("public static final String CREATE_TABLE = \"CREATE TABLE IF NOT EXISTS \"\n" +
"			+ TABLE_NAME\n" +
"			+ \" (\"\n");
         for(i = 0; i < sz; i++){
            Field fld = fields[i];
            String f = fld.getName();
            Class<?> ftype = fld.getType();
            String t = ftype.getCanonicalName();
            String stype = ftype.getSimpleName();
            
            // Get constant 
            String cst = field2const.get(f);
            String sqltype = "TEXT";
            boolean last = (i+1) == sz;
            
            // special serializers
            if ("byte[]".equalsIgnoreCase(t)){
                sqltype="BLOB";
            } else if( "java.util.Date".equalsIgnoreCase(t)
                    || "java.lang.Integer".equalsIgnoreCase(t)
                    || "java.lang.Long".equalsIgnoreCase(t)){
                sqltype="INTEGER DEFAULT 0";
                
            } else if("java.lang.Double".equalsIgnoreCase(t)
                    ||"java.lang.Float".equalsIgnoreCase(t)
                    ||"java.lang.String".equalsIgnoreCase(t)){
                sqltype="TEXT";
            }
            
            String pcst = fill(cst, maxConstLen + 1, " ");
            
            // Special case for primary key.
            if ("id".equalsIgnoreCase(f)){
                sb.append("				+ " + pcst + " \t + \" " + sqltype + " PRIMARY KEY AUTOINCREMENT");
                if (!last) sb.append(", ");
                sb.append("\"\n");
                
                continue;
            }
            
            // Another attributes.
            sb.append("				+ " + pcst + " \t + \" " + sqltype);
            if (!last) sb.append(", ");
            sb.append("\"\n");
        }
        sb.append("                 + \");\";\n\n"); 
        
        // URI
        sb.append(indent).append("/**\n");
        sb.append(indent).append(" * URI for content provider.<br/>\n");
        sb.append(indent).append(" */\n");
        sb.append(indent).append("public static final Uri URI = Uri.parse(ContentResolver.SCHEME_CONTENT + \"://\"\n");
        sb.append(indent).append("        + SipManager.AUTHORITY + \"/\" + TABLE_NAME);\n\n");
        sb.append(indent).append("/**\n");
        sb.append(indent).append(" * Base URI for contact content provider.<br/>\n");
        sb.append(indent).append(" * To append with {@link #FIELD_ID}\n");
        sb.append(indent).append(" * \n");
        sb.append(indent).append(" * @see ContentUris#appendId(android.net.Uri.Builder, long)\n");
        sb.append(indent).append(" */\n");
        sb.append(indent).append("public static final Uri ID_URI_BASE = Uri.parse(ContentResolver.SCHEME_CONTENT + \"://\"\n");
        sb.append(indent).append("        + SipManager.AUTHORITY + \"/\" + TABLE_NAME + \"/\");\n\n");
        
        // Constructors
        sb.append(indent).append("public ").append(en.getSimpleName()).append("() {\n\n").append(indent).append("}\n\n");
        
        // Constructor from cursor
        sb.append(indent).append("/**\n" +
"     * Construct a "+en.getSimpleName()+" from a cursor retrieved with a\n" +
"     * {@link ContentProvider} query on {@link #TABLE_NAME}.\n" +
"     * \n" +
"     * @param c the cursor to unpack\n" +
"     */\n");
        sb.append(indent).append("public ").append(en.getSimpleName()).append("(Cursor c) {\n");
        sb.append(indent).append("    super();\n");
        sb.append(indent).append("    createFromDb(c);\n    }\n\n");
        
        // Construct from parcel
        sb.append(indent).append("/**\n" +
"     * Construct from parcelable <br/>\n" +
"     * Only used by {@link #CREATOR}\n" +
"     * \n" +
"     * @param in parcelable to build from\n" +
"     */\n" +
"    public "+en.getSimpleName()+"(Parcel in) {\n");
        for(i = 0; i < sz; i++){
            Field fld = fields[i];
            String f = fld.getName();
            Class<?> ftype = fld.getType();
            String t = ftype.getCanonicalName();
            String stype = ftype.getSimpleName();
            
            // special serializers
            if ("byte[]".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append("in.readByteArray("+f+");\n");
            } else if("java.lang.String".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append(f).append(" = getReadParcelableString(in.readString());\n");
            } else if("java.util.Date".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append(f).append(" = new Date(in.readLong());\n");
            } else if("java.lang.Boolean".equalsIgnoreCase(t) || "boolean".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append(f).append(" = (in.readInt() != 0) ? true:false;\n");
            } else if("java.lang.Integer".equalsIgnoreCase(t) || "int".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append(f).append(" = in.readInt();\n");
            } else if("java.lang.Long".equalsIgnoreCase(t) || "long".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append(f).append(" = in.readLong();\n");
            } else {
                System.err.println("!! Unrecognized field type: " + t);
            }
        }
        
        sb.append(indent).append("}\n\n");
        
        // Always constant parcelable creator
        sb.append(indent).append("/**\n" +
"     * Parcelable creator. So that it can be passed as an argument of the aidl\n" +
"     * interface\n" +
"     */\n" +
"    public static final Parcelable.Creator<"+en.getSimpleName()+"> CREATOR = new Parcelable.Creator<"+en.getSimpleName()+">() {\n" +
"        public "+en.getSimpleName()+" createFromParcel(Parcel in) {\n" +
"            return new "+en.getSimpleName()+"(in);\n" +
"        }\n" +
"\n" +
"        public "+en.getSimpleName()+"[] newArray(int size) {\n" +
"            return new "+en.getSimpleName()+"[size];\n" +
"        }\n" +
"    };\n" +
"\n" +
"    /**\n" +
"     * @see Parcelable#describeContents()\n" +
"     */\n" +
"    @Override\n" +
"    public int describeContents() {\n" +
"        return 0;\n" +
"    }\n\n" +
"    /**\n" +
"     * @see Parcelable#writeToParcel(Parcel, int)\n" +
"     */\n" +
"    @Override\n" +
"    public void writeToParcel(Parcel dest, int flags) {\n");
        for(i = 0; i < sz; i++){
            Field fld = fields[i];
            String f = fld.getName();
            Class<?> ftype = fld.getType();
            String t = ftype.getCanonicalName();
            String stype = ftype.getSimpleName();
            
            // special serializers
            if ("byte[]".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append("dest.writeByteArray(");
                sb.append(f+" != null ? "+f+" : new byte[0]);\n");
            } else if("java.lang.String".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append("dest.writeString(getWriteParcelableString("+f+"));\n");
            } else if("java.util.Date".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append("dest.writeLong("+f+" != null ? "+f+".getTime() : 0);\n");
            } else if("java.lang.Boolean".equalsIgnoreCase(t) || "boolean".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append("dest.writeInt("+f+" ? 1 : 0);\n");
            } else if("java.lang.Integer".equalsIgnoreCase(t) || "int".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append("dest.writeInt("+f+" != null ? "+f+" : 0);\n");
            } else if("java.lang.Long".equalsIgnoreCase(t) || "long".equalsIgnoreCase(t)){
                sb.append(indent).append(indent).append("dest.writeLong("+f+");\n");
            } else {
                System.err.println("!! Unrecognized field type: " + t);
            }
        }
        sb.append(indent).append("}\n\n");
        
        // Helper methods
        sb.append(indent).append("private String getWriteParcelableString(String str) {\n" +
"        return (str == null) ? \"null\" : str;\n" +
"    }\n" +
"\n" +
"    private String getReadParcelableString(String str) {\n" +
"        return str.equalsIgnoreCase(\"null\") ? null : str;\n" +
"    }\n" +
"    \n" +
"    private Date getReadParcelableDate(long lng) {\n" +
"    	return lng==0 ? null : new Date(lng);\n" +
"    }\n" +
"\n" +
"    /**\n" +
"     * Create account wrapper with cursor data.\n" +
"     * \n" +
"     * @param c cursor on the database\n" +
"     */\n" +
"    private final void createFromDb(Cursor c) {\n" +
"        ContentValues args = new ContentValues();\n" +
"        \n" +
"        try {\n" +
"        	DatabaseUtils.cursorRowToContentValues(c, args);\n" +
"        	createFromContentValue(args);\n" +
"        } catch(Exception e){\n" +
"        	Log.w(THIS_FILE, \"Cannot load to content values, falling back to default\", e);\n" +
"        	this.createFromCursor(c);\n" +
"        }\n" +
"    }\n\n" + 
"    private final void createFromCursor(Cursor c){\n" +
"        int colCount = c.getColumnCount();\n" +
"        for(int i=0; i<colCount; i++){\n" +
"            final String colname = c.getColumnName(i);\n");
        for(i = 0; i < sz; i++){
            Field fld = fields[i];
            String f = fld.getName();
            Class<?> ftype = fld.getType();
            String t = ftype.getCanonicalName();
            String stype = ftype.getSimpleName();
            
            // Get constant 
            String cst = field2const.get(f);
            if (i==0){
                sb.append("            if (" + cst + ".equals(colname)){\n");
            } else {
                sb.append("            } else if (" + cst + ".equals(colname)){\n");
            }
            
            // special serializers
            if ("byte[]".equalsIgnoreCase(t)){
                sb.append("                this." + f + " = c.getBlob(i);\n");
            } else if("java.lang.String".equalsIgnoreCase(t)){
                sb.append("                this." + f + " = c.getString(i);\n");
            } else if("java.util.Date".equalsIgnoreCase(t)){
                sb.append("                this." + f + " = new Date(c.getLong(i));\n");
            } else if("java.lang.Boolean".equalsIgnoreCase(t) || "boolean".equalsIgnoreCase(t)){
                sb.append("                this." + f + " = c.getInt(i) != 0;\n");
            } else if("java.lang.Integer".equalsIgnoreCase(t) || "int".equalsIgnoreCase(t)){
                sb.append("                this." + f + " = c.getInt(i);\n");
            } else if("java.lang.Long".equalsIgnoreCase(t) || "long".equalsIgnoreCase(t)){
                sb.append("                this." + f + " = c.getLong(i);\n");
            } else {
                System.err.println("!! Unrecognized field type: " + t);
            }
        }
        sb.append("            } else {\n");
        sb.append("                Log.w(THIS_FILE, \"Unknown column name: \" + colname);\n");
        sb.append("            }\n");
        sb.append("        }\n");
        sb.append(indent).append("}\n\n");
        
        // Create from content value
        sb.append(indent).append("/**\n" +
"     * Create object with content values pairs.\n" +
"     * \n" +
"     * @param args the content value to unpack.\n" +
"     */\n" +
"    private final void createFromContentValue(ContentValues args) {\n");
        // Temporary variables for this
        if (hasInteger)
            sb.append("        Integer tmp_i;\n");
        if (hasLong)
            sb.append("        Long tmp_l;\n");
        if (hasString)
            sb.append("        String tmp_s;\n");
        if (hasByteArray)
            sb.append("        byte[] tmp_b;\n");
        sb.append("\n");
        
        for(i = 0; i < sz; i++){
            Field fld = fields[i];
            String f = fld.getName();
            Class<?> ftype = fld.getType();
            String t = ftype.getCanonicalName();
            String stype = ftype.getSimpleName();
            
            // Get constant 
            String cst = field2const.get(f);
            
            // special serializers
            if ("byte[]".equalsIgnoreCase(t)){
                sb.append(indent).append("    tmp_b = args.getAsByteArray("+cst+");\n");
                sb.append(indent).append("    if (tmp_b != null) {\n");
                sb.append(indent).append("        this."+f+" = tmp_b;\n");
                sb.append(indent).append("    }\n");
            } else if("java.lang.String".equalsIgnoreCase(t)){
                sb.append(indent).append("    tmp_s = args.getAsString("+cst+");\n");
                sb.append(indent).append("    if (tmp_s != null) {\n");
                sb.append(indent).append("        this."+f+" = tmp_s;\n");
                sb.append(indent).append("    }\n");
            } else if("java.util.Date".equalsIgnoreCase(t)){
                sb.append(indent).append("    tmp_l = args.getAsLong("+cst+");\n");
                sb.append(indent).append("    if (tmp_l != null) {\n");
                sb.append(indent).append("        this."+f+" = new Date(tmp_l);\n");
                sb.append(indent).append("    }\n");
            } else if("java.lang.Boolean".equalsIgnoreCase(t) || "boolean".equalsIgnoreCase(t)){
                sb.append(indent).append("    tmp_i = args.getAsInteger("+cst+");\n");
                sb.append(indent).append("    if (tmp_i != null) {\n");
                sb.append(indent).append("        this."+f+" = tmp_i !=0;\n");
                sb.append(indent).append("    }\n");
            } else if("java.lang.Integer".equalsIgnoreCase(t) || "int".equalsIgnoreCase(t)){
                sb.append(indent).append("    tmp_i = args.getAsInteger("+cst+");\n");
                sb.append(indent).append("    if (tmp_i != null) {\n");
                sb.append(indent).append("        this."+f+" = tmp_i;\n");
                sb.append(indent).append("    }\n");
            } else if("java.lang.Long".equalsIgnoreCase(t) || "long".equalsIgnoreCase(t)){
                sb.append(indent).append("    tmp_l = args.getAsLong("+cst+");\n");
                sb.append(indent).append("    if (tmp_l != null) {\n");
                sb.append(indent).append("        this."+f+" = tmp_l;\n");
                sb.append(indent).append("    }\n");
            } else {
                System.err.println("!! Unrecognized field type: " + t);
            }
        }
        sb.append(indent).append("}\n\n");
        
        // Create content values from object.
        sb.append(indent).append("public ContentValues getDbContentValues() {\n" +
"        ContentValues args = new ContentValues();\n");
        for(i = 0; i < sz; i++){
            Field fld = fields[i];
            String f = fld.getName();
            Class<?> ftype = fld.getType();
            String t = ftype.getCanonicalName();
            String stype = ftype.getSimpleName();
            
            // Special case for id
            if ("id".equalsIgnoreCase(f)){
                sb.append("        if (id!=null && id!=INVALID_ID) {\n" +
"            args.put(FIELD_ID, id);\n" +
"        }\n");
                continue;
            }
            
            // Get constant 
            String cst = field2const.get(f);
            
            // Nullable object?
            boolean nullable = true;
            if ("int".equalsIgnoreCase(t) 
                    || "long".equalsIgnoreCase(t)
                    || "byte".equalsIgnoreCase(t)
                    || "char".equalsIgnoreCase(t)
                    || "boolean".equalsIgnoreCase(t)){
                nullable=false;
            }
            
            if (nullable){
                sb.append("        if (this." + f + " != null) \n");
                
                 if("java.util.Date".equalsIgnoreCase(t)){
                    sb.append("            args.put("+cst+", this."+f+".getTime());\n");
                 } else {
                    sb.append("            args.put("+cst+", this."+f+");\n");
                 }
            } else {
                sb.append("        args.put("+cst+", this."+f+");\n");
            }
        }
        sb.append("        return args;\n");
        sb.append(indent).append("}\n\n");
        
        // Getters & Setters
        for(Field fld : fields){
            Class<?> type = fld.getType();
            final String typeName = type.getSimpleName(); //type.getCanonicalName().replace(pack, packDest);
            String curType = typeName;
            sb.append(getterSetter(fld, curType)).append("\n");
        }
          
        // end class
        sb.append("}\n\n</CLASS>\n\n");
        return sb.toString();
    }
    
    public static String camelCase2Underscore(String input){
       return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, input);
    }
    
    /**
     * Generates getter setter 
     * @param fld
     * @return 
     */
    public static String getterSetter(Field fld, String tc){
        //StringBuilder sb = new StringBuilder();
        Class<?> ftype = fld.getType();
        String n = fld.getName();
        String N = Character.toUpperCase(n.charAt(0)) + n.substring(1);
        //String tc = ftype.getCanonicalName().replace(pack, packDest);
        
        return getterSetterRaw(n, N, tc);
    }
    
    /**
     * Generate raw getter and setter from passed variables
     * @param n attribute name
     * @param N function name for getter and setter - attribute suffix, first upper
     * @param tc type of attribute
     * @return 
     */
    public static String getterSetterRaw(String n, String N, String tc){
        StringBuilder sb = new StringBuilder();
        sb.append(
"    /**\n" +
"     * Gets the value of the "+n+" property.\n" +
"     * \n" +
"     * @return\n" +
"     *     possible object is\n" +
"     *     {@link "+tc+" }\n" +
"     *     \n" +
"     */\n" +
"    public "+tc+" get"+N+"() {\n" +
"        return "+n+";\n" +
"    }\n" +
"\n" +
"    /**\n" +
"     * Sets the value of the "+n+" property.\n" +
"     * \n" +
"     * @param value\n" +
"     *     allowed object is\n" +
"     *     {@link "+tc+" }\n" +
"     *     \n" +
"     */\n" +
"    public void set"+N+"("+tc+" value) {\n" +
"        this."+n+" = value;\n" +
"    }");
        
        return sb.toString();
    }
    
    public static String fill(int length, String with) {
        StringBuilder sb = new StringBuilder(length);
        while (sb.length() < length) {
            sb.append(with);
        }
        return sb.toString();
    }
    
    public static String fill(String value, int length, String with) {
        StringBuilder result = new StringBuilder(length);
        result.append(value);
        result.append(fill(Math.max(0, length - value.length()), with));

        return result.toString();
    }
}
