/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.fi.xklinec.phoenixdbgen.db;

import java.util.Date;

/**
 *
 * @author ph4r05
 */
public class CertificateHistory {
    
    protected String owner;
    protected String certHash;
    protected String certIssuerCn;
    protected String certIssuerId;
    protected Integer certSerial;
    protected Date dateNotBefore;
    protected Date dateNotAfter;
    protected Date firstSeen;
    protected Date lastSeen;
    
}
