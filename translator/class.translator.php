<?php
/**
 * @author              Dusan Klinec Ph4r05
 * @copyright           Copyright (C) 2005 - 2010 Dusan Klinec Ph4r05, Net-Wings Solutions. All rights reserved.
 * @license		GNU/GPL
 * This is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

require_once('class.googleTranslate.php');

class translator 
{
    private $gw=false;

    const ENCODING = 'UTF-8';
    const TLIMIT = 1400;

    private $src = 'en';
    private $dst = 'cs';
    public $res_dir;
    public $trans = array();
    public $stringsFiles = array();

    public function __construct()
    {
        setlocale(LC_ALL, "cs_CZ.UTF-8", 'cs_CS.utf8', 'cs_CS.UTF-8');
        mb_internal_encoding(self::ENCODING);

        $this->gw = new GoogleTranslateWrapper();
     }

    public function setTranslateLanguages($src, $dst)
    {
        $this->src = $src;
        $this->dst = $dst;
    }

    public function translate3($string, $src, $dst)
    {

        $this->gw = new GoogleTranslateWrapper();
	$this->gw->setProxy(false);
        //$this->gw->setProxy('127.0.0.1:3128');
        $this->gw->setCredentials('AIzaSyA74VO99vQGmHSk1SPaFqrVmLdiJvo57Jc', '');//'89.29.122.93');
        //$this->gw->setReferrer('89.29.122.93');
        $translated = $this->gw->translateApi($string, $dst, $src);
        
        if ($this->gw->isSuccess())
        {
            return $translated;
        }
        else
        {	
	    echo "Problem with translator\n";
            return false;
        }

    }
   
    /**
     * Loads android *strings* XML file, returns key-value pairs for found translate strings
     */
    public function loadStringFilesDir($dir){
	$files = array();
	$dc = scandir($dir);
	foreach($dc as $c){

		if (preg_match('/\.xml$/', $c)==false) continue;
		if (strpos($c, 'strings')===false) continue;		
		
		$files[] = $c;
	}
		
	return $files;
    }

    /**
     * Load all files in /res/values having "strings" substring, ending on .xml
     */
    public function loadStringsFiles(){
	$this->stringsFiles = $this->loadStringFilesDir($this->res_dir . '/values');
	
	echo "found strings files:\n";
	print_r($this->stringsFiles);
    }

    /**
     * Loads all values* from res/ directory
     */
    public function loadValues(){
	if (!file_exists($this->res_dir . '/values')){
		throw new Exception('Directory with default language was not found');
	}

	// PHP5 scandir
	$dc = scandir($this->res_dir);
	foreach($dc as $dir){
		if (strpos($dir, 'values-')===false) continue;
		$ct = str_replace('values-', '', $dir);
		if (preg_match('/s?v|w\d+/', $ct)) continue;
		if (	strpos($ct, 'land')!==false
			|| strpos($ct, 'large')!==false
			|| strpos($ct, 'small')!==false
			|| strpos($ct, 'cs')!==false
			|| strpos($ct, 'cz')!==false
			|| strpos($ct, 'es-rES')!==false
		    ) continue;
		
		
		$this->trans[] = $ct;
	}
	
	echo "found translations: \n";
	print_r($this->trans);
    }

    /**
     * Parse strings from XML file
     */
    public function parseStrings($data){
	$match = array();
	$result = array();
	if (preg_match_all('/<string\s+name=\"(.+?)\">(.*?)<\/string>/', $data, $match)==false) return $result;
	
	if (empty($match) || !isset($match[1]) || !isset($match[2])) return $result;
	foreach($match[1] as $id => $key){
		$result[$key] = $match[2][$id];
	}
	
	return $result;
    }

    public function escape($x){
	$res = str_replace('\'', '\\\'', $x);
	$res = str_replace('"', '\\"', $res);
//	$res = str_replace('"', '\\"', $x);
//	$res = str_replace('<', '&lt;', $x);
//	$res = str_replace('>', '&gt;', $x);

	// fix artificial space added
	$res = preg_replace('/\\\[\s](.)/', '\\\$1', $res);
	$res = str_replace('% S', '%s', $res);
	$res = str_replace(' % 1 $ s ', '%1$s', $res);
	$res = str_replace('\N', '\n', $res);
	$res = preg_replace('/\\\\n ([a-zA-Z0-9])/', '\\n$1', $res);
	$res = preg_replace('/& /', '&amp; ', $res);
	
	return $res;
    }

    public function work(){
	$this->loadValues();
	$this->loadStringsFiles();
	$cdef = $this->res_dir . '/values';
	
	var_dump($this->translate3("Hello world!", "en", "cs"));
	var_dump($this->gw->getLastError());

	// iterate over translations
	foreach($this->trans as $ct){
		$cdir = $this->res_dir . '/values-' . $ct;
		echo "Current translation [$ct] dir [$cdir]\n";
		$curSF = $this->loadStringFilesDir($cdir);
		
		// Logic changed here. Translations often contain only subset of string files. It may happen that 
		// values/ contain files that are not contained in values-lang. Thus iterate over source files
		// and find corresponding files in language dir.
		foreach($this->stringsFiles as $sourceStringFile){
			echo " source file in /res/values/ [$sourceStringFile]\n";
			$fcor = $cdef . '/' . $sourceStringFile;
			$sdata = file_get_contents($fcor);
			$spar = $this->parseStrings($sdata);

			// remove useless translations
			if (in_array($sourceStringFile, array('wizard_sipgate_strings.xml', 'xliff_strings.xml', 'wizard_sipme_strings.xml', 'wizard_sonetel_strings.xml', 'wizard_ovh_strings.xml',
					'wizard_onsip_strings.xml', 'wizard_iinet_strings.xml', 'wizard_fritz_strings.xml', 'wizard_cryptel_strings.xml', 'wizard_ecs_strings.xml', 
					'wizard_callcentric_strings.xml'))){
				echo "Removing...\n";
				@unlink($cdir . '/' . $sourceStringFile);
				continue;
			}
			
			// find corresponding file
			$curSFile = $cdir . '/' . $sourceStringFile;
			$corExists = file_exists($curSFile);
			$dparNew = array();
			$dparStr = array();
			$data = '';
			$dpar = array();
			if ($corExists){
				$data = file_get_contents($curSFile);
				$dpar = $this->parseStrings($data);
			}
			
			// Iterate over source values, find in translated values.
 			// If value is not found in destination translation, it has to be
			// translated with Google API.
			foreach($spar as $key=>$val){
				if (isset($dpar[$key])) continue;
				echo "key [$key] not found in [$ct] [$sourceStringFile], have to translate\n";
				// prepare for translation
				$toTran = str_replace("\'", "'", $val);
				$toTran = str_replace('\"', '"', $toTran);
				$toTran = preg_replace("/\\\\n([a-zA-Z0-9])/", "\\n $1", $toTran);
	
				$dstLang = $ct;
				if ($dstLang=='zh-rCN') $dstLang='zh-CN';
				if ($dstLang=='zh-rTW') $dstLang='zh-TW';
				$res = $this->translate3($toTran, 'en', $dstLang);
				if ($res===false){
					echo "!! Something wrong with the translator; quit\n";
					break;
				}
				
				echo "Translated successfully; [$val] = [".$this->escape($res)."]\n";
				$dparNew[$key] = $res;
				$dpar[$key] = $res;
				$dparStr[] = '<string name="'.$key.'">'.$this->escape($res).'</string>';

//				sleep(1);
			}

			// write new translations to file
			if (!empty($dparNew)){
				echo "--> Going to dump new translations to file, count=" . count($dparNew) . "\n\n\n";
				
				$newString = implode("\n", $dparStr);
				if ($corExists){			
					file_put_contents($curSFile, "\n\n<!-- ##automatically translated below -->\n" . $newString, FILE_APPEND);
				} else {
					file_put_contents($curSFile, "<?xml version='1.0' encoding='UTF-8'?>\n<resources>\n\n<!-- ##automatically translated below -->\n" . $newString . "\n</resources>");
				}
				sleep(5);
			}

			// fix stored file
			$data = file_get_contents($curSFile);
			$data = trim(str_replace('</resources>', '', $data));
			if (!empty($data)){
				$data = trim($data) . "\n</resources>";
			
				// fix \
				$data = str_replace('\ N', '\n', $data);
				$data = str_replace('\N', '\n', $data);

				$data = preg_replace('/\\\[\s](.)/', '\\\$1', $data);
				$data = str_replace(' % S ', '%s', $data);
				$data = str_replace('% S', '%s', $data);
				$data = str_replace('% s', '%s', $data);
				$data = str_replace(' \"% s \"', '\"%s\"', $data);
				$data = str_replace(' % 1 $ s ', '%1$s', $data);
				$data = str_replace('% 1 $ s', '%1$s', $data);

				$data = str_replace('<sip:login@registrar&gt;"', '&lt;sip:login@registrar>"', $data);
				$data = str_replace('<sip:login@registrar>"'   , '&lt;sip:login@registrar>"', $data);
				$data = str_replace('&lt;sip:login@registrar>"'   , '&lt;sip:login@registrar&gt;"', $data);


				file_put_contents($curSFile, trim($data));
			}
		}		
	}
    }
}


$tr = new translator();
$tr->res_dir = dirname(__FILE__) . '/../res';
$tr->work();



