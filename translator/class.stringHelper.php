<?php

class stringHelper
{
    /**
     * Helper method - first letter UPPERCASE
     *
     * @param <type> $str
     * @return <type>
     * @return <boolean>
     */
    public static function ucfirst_utf8($str, $eachword=false)
    {
        if (mb_check_encoding($str,'UTF-8')) {
            
            // multiword ?
            if ($eachword && mb_strpos($str, ' ') !==false)
            {
                $words = mb_split(' ', $str);
                $result = array();
                foreach($words as $id=>$w)
                {
                    $result[] = self::ucfirst_utf8($w, false);
                }

                return implode(' ', $result);
            }

            $first = mb_substr(
                mb_strtoupper($str, "utf-8"),0,1,'utf-8'
            );
            return $first.mb_substr(
                mb_strtolower($str,"utf-8"),1,mb_strlen($str),'utf-8'
            );
        } else {
            return $str;
        }
    }

    /**
     * Helper method generating absolute path
     *
     * @param <type> $path
     * @return <type>
     */
     public static function get_absolute_path($path)
     {
        //protocol
        $proto = array();
        if (preg_match('/^([a-zA-z]+:\/\/)/', $path, $proto))
        {
            $path = str_replace($proto[1], '', $path);
        }

            $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
            $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
            $absolutes = array();
            foreach ($parts as $part) {
                if ('.' == $part) continue;
                if ('..' == $part) {
                    array_pop($absolutes);
                } else {
                    $absolutes[] = $part;
                }
            }
            $path = implode(DIRECTORY_SEPARATOR, $absolutes);
        if (!empty($proto)) $path = $proto[1] . $path;
        return $path;
    }

    /**
     * escapes string to use as regular expression
     */
    public static function escape_string_for_regex($str)
    {
            //All regex special chars (according to arkani at iol dot pt below):
            // \ ^ . $ | ( ) [ ]
            // * + ? { } ,

            return preg_quote($str, '/');
    }

    /**
     * @args string $text line of encoded text
     *       string $from_enc (encoding type of $text, e.g. UTF-8, ISO-8859-1)
     *
     * @returns 7bit representation
     */
    public static function to7bit($text,$from_enc) {
        //$text = mb_convert_encoding($text,'HTML-ENTITIES',$from_enc);
          $text = iconv($from_enc, 'US-ASCII//TRANSLIT', $text);
    //     $text = preg_replace(
    //         array('/&szlig;/','/&(..)lig;/',
    //              '/&([aouAOU])uml;/','/&(.)[^;]*;/'),
    //         array('ss',"$1","$1".'e',"$1"),
    //         $text);
        return $text;
    }

    /**
     * return number of words in string
     *
     * @param <type> $string
     * @return <type>
     */
    public static function countWords($string)
    {
       $w = preg_replace('/\s+|\t|\n/', ' ', $string);
       return (substr_count($w, ' ')+1);
    }


    /**
     * Expands string without spaces written in style
     *  helloWorldAgain -> hello World Again
     *
     * @param <string> $string
     * @return <string> output
     */
    public static function caseExpand($string)
    {
        return preg_replace('/([a-z])([A-Z])/', '\1 \2', $string);
    }

    /**
     * Normalizes spaces (2 and more spaces will be replaced by 1 )
     *
     * @param <string> $string
     */
    public static function normalizeSpaces($string)
    {
        return preg_replace(array('/\s{2,}/', '/\t{2,}/', '/\n{2,}/'), array(' ', "\t", "\n"), $string);
    }

    /**
     * converts <br/> tags to \n
     *
     * @param <type> $string
     */
    public static function br2nl($string,$extended=false)
    {
        $return = preg_replace('/(<br\/?>)/', "\n", $string);
        if ($extended)
        {
            $return = preg_replace('/(<hr\/?>)/', "\n", $return);
            $return = preg_replace('/(?:<p(?:[^>]+)?>).+?(?:<\/?p(?:[\s]+)?>)/', "\n\1\n", $return);
        }

        return trim($return);
    }

    /**
     * Expands doubled case form
     * Example: DBlue -> D Blue
     *
     * @param <string> $string
     * @return <string>
     */
    public static function expandDoubledCase($string)
    {
        return preg_replace('/([A-Z])([A-Z])([a-z])/', '\1 \2\3', $string);
    }

    /**
     * Converts character to lower
     *
     * @param <type> $string
     * @param <type> $encoding
     * @return <type>
     */
    public static function toLower($string, $encoding='UTF-8')
    {
        return mb_strtolower($string, $encoding);
    }

    /**
     * Converts character to upper
     *
     * @param <type> $string
     * @param <type> $encoding
     * @return <type>
     */
    public static function toUpper($string, $encoding='UTF-8')
    {
        return mb_strtoupper($string, $encoding);
    }

    /**
     * Uses preg_split on multiple patterns
     * Usable if one composite pattern would be too long
     * (non-recursive version)
     *
     * @param array $patterns
     * @param <type> $pieces
     * @return <type> pieces
     */
    public static function multi_reg_split($patterns, $pieces)
    {
        // we will use queue for this purpose
        $q = new Queue();
        $q->Clear();

        $out = array();

        if (!is_array($pieces))
        {
            $pieces = array($pieces);
        }

        // push all pieces to the queue
        foreach($pieces as $piece)
        {
            $q->Put($piece);
        }

        while(!$q->IsEmpty())
        {
            $piece = $q->Get();
//            echo "piece: $piece \n";

            $splitted = false;
            foreach($patterns as $pattern)
            {
                $sub = preg_split($pattern, $piece, -1, PREG_SPLIT_NO_EMPTY);
                if (!empty($sub))
                {
                    $changed = false;

                    // push pieces to the queue
                    foreach($sub as $p)
                    {
                        $pT = trim($p);

                        if ($p == $piece || empty($pT)) continue;
                        //echo "XPiece: $p\n";
                        $changed = true;
                        $q->Put($pT);
                    }

                    // now we need to do id all over again
                    if ($changed)
                    {
                        $splitted = true;
                        break;
                    }
                }
            }

            if (!$splitted)
            {
                $out[] = $piece;
            }
        }

        return $out;
    }

    /**
     * Generates sequences of $len consecutive words from string
     *
     * @param <type> $string
     * @param <type> $len
     * @return <type>
     */
    public static function genFrames($string, $len)
    {
        // normalize
        $string = self::normalizeSpaces($string);

        $expl = preg_split('/[.,\/\s]+/im', $string, -1, PREG_SPLIT_NO_EMPTY);

        // trim pieces
        array_map('trim', $expl);
        
        // drop empty
        self::dropEmptyPiece($expl);

        // reindexation
        $expl = array_merge($expl, array());
        $cn   = sizeof($expl);

        $outFrame = array();
        for ($i = 0; $i < $cn - $len + 1; $i++)
        {
            $work = array();
            for($j = $i, $c = 0; $c < $len; $c++, $j++)
            {
                $work[] = $expl[$j];
            }

            $outFrame[] = implode(' ', $work);
        }

        sort($outFrame);
        return $outFrame;
    }

    /**
     * Drops empty elements from array keeping only non-empty
     *
     * @param <type> $arr
     */
    public static function dropEmptyPiece(&$arr)
    {
        foreach($arr as $key => $val)
        {
            if (empty($val))
            {
                unset($arr[$key]);
                continue;
            }

            if ($nonTranslate)
            {
                $arr[$key] = $this->stripOutNontranslate($arr[$key]);
                if (empty($arr[$key])) unset($arr[$key]);
            }
        }
    }

    /**
     * Decodes html strings
     *
     * @param <type> $string
     * @return <type>
     */
    public static function decodeHtml($string)
    {
        $string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
        $string = preg_replace('~&#x0*([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $string);
        $string = preg_replace('~&#0*([0-9]+);~e', 'chr(\\1)', $string);
        return $string;
    }
}